$(function(){
	$("#formulario").validate({
		rules: {
			
			user: {
				required: true,
				minlength: 3
			},
			email: {
				required: true,
				email: true
			},
			
			direccion: {
				required: true
			},
			
			edad: {
				required: true,
				number: true,
				min: 18
			},
			
			mensaje: {
				required: true
			}
		},
		
		messages: {
			user: {
				required: 'Ingrese su nombre!',
				minlength: 'Minimo 3 caracteres'
			},
			
			email: {
				required: 'Ingrese su correo!',
				minlength: 'Correo no valido'
			},
			
			direccion: {
				required: 'Ingrese direccion'
			},
			
			edad: {
				required: 'Ingrese edad',
				number: 'Solo valores numericos',
				minlength: 'Solo mayores de 18 años'
			},
			
			mensaje: {
				required: 'Comentario obligatorio'
			}
			
		}
	});
	
});

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display="block";
  dots[slideIndex-1].className+=" active";
}

