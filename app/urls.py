from django.urls import path
from .views import home, contacto, galeria, gal

urlpatterns = [
    path('', home, name="home"),
    path('galeria/', galeria, name="galeria"),
    path('contacto/', contacto, name="contacto"),
    path('gal/', gal, name="gal")
]
